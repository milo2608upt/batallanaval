/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batallanaval;

import java.util.Scanner;

/**
 *
 * @author Emilio
 */
public class BatallaNaval {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        int cont=0;
        System.out.println("tamaño del tablero");
        int ta=teclado.nextInt();
        int jugador1[]=new int[ta];
        int jugador2[]=new int[ta];
        for(int i=0;i<ta;i++){
            jugador1[i]=0;
            jugador2[i]=0;
        }
        System.out.println("Jugador 1");
        String jugador_uno=teclado.next();
        System.out.println("Jugador 2");
        String jugador_dos=teclado.next();
        
        System.out.println("---"+jugador_uno+"---");
        
        System.out.println("1.- Colocar Barco");
        System.out.println("2.- Terminar Turno");
        int op;
        do{
            System.out.println("\n");
        System.out.println("Opcion: ");
        op=teclado.nextInt();
        
        switch(op){
            case 1:
                for(int i=0;i<jugador1.length;i++){
                    System.out.print("["+jugador1[i]+"]");
                }
                System.out.println("\n");
                System.out.println("Posicion");
                int po=teclado.nextInt();
                if(po<=ta){
                    jugador1[po-1]=1;
                for(int i=0;i<jugador1.length;i++){
                    System.out.print("["+jugador1[i]+"]");
                }
                }else{
                    System.out.print("[COLOCA POSICION VALIDA]");
                }
                break;
        }
        }while(op!=2);
        
        System.out.println("---"+jugador_dos+"---");
        
        System.out.println("1.- Colocar Barco");
        System.out.println("2.- Terminar Turno");
        do{
            System.out.println("\n");
        System.out.println("Opcion: ");
        op=teclado.nextInt();
        
        switch(op){
            case 1:
                for(int i=0;i<jugador2.length;i++){
                    System.out.print("["+jugador2[i]+"]");
                }
                System.out.println("\n");
                System.out.println("Posicion");
                int po=teclado.nextInt();
                if(po<=ta){
                    jugador2[po-1]=1;
                for(int i=0;i<jugador2.length;i++){
                    System.out.print("["+jugador2[i]+"]");
                }
                }else{
                    System.out.print("[COLOCA POSICION VALIDA]");
                }
                
                break;
        }
        }while(op!=2);
        
        do{
        System.out.println("Ataca "+jugador_uno);
        
         System.out.println("Posicion de la nave:");
         int nav=teclado.nextInt();
         
         if(jugador2[nav-1]==1){
             jugador2[nav-1]=0;
             System.out.println("Has derrivado una nave");
         }else{
             System.out.println("Has fallado");
         }
         
         for(int i=0;i<jugador2.length;i++){
                    
                    if(jugador2[i]==0){
                        cont=cont+1;
                    }
                }
         if(cont==ta){
             System.out.println("Juego terminado"+jugador_uno+" gana");
         }else{
             cont=0;
             System.out.println("Ataca"+jugador_dos);
        
            System.out.println("Posicion de la nave:");
            int nav2=teclado.nextInt();

            if(jugador1[nav2-1]==1){
                jugador1[nav2-1]=0;
                System.out.println("Has derrivado una nave");
            }else{
             System.out.println("Has fallado");
         }
            
            for(int i=0;i<jugador1.length;i++){
                    if(jugador1[i]==0){
                        cont=cont+1;
                    }
                }
         if(cont==ta){
             System.out.println("Juego terminado "+jugador_dos+" gana");
         }else{
             cont=0;
         }
         }
        }while(cont!=ta);
    }

}
